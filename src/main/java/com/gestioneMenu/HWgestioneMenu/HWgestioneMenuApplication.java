package com.gestioneMenu.HWgestioneMenu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HWgestioneMenuApplication {

	public static void main(String[] args) {
		SpringApplication.run(HWgestioneMenuApplication.class, args);
	}

}
