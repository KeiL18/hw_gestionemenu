package com.gestioneMenu.HWgestioneMenu.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.gestioneMenu.HWgestioneMenu.connessione.ConnettoreDB;
import com.gestioneMenu.HWgestioneMenu.model.Bevanda;
import com.gestioneMenu.HWgestioneMenu.model.Piatto;
import com.mysql.jdbc.PreparedStatement;

public class BevandaDAO implements Dao<Bevanda>{

	@Override
	public Bevanda getById(int ID) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT id, nome, ingredienti, codice, prezzo, volume FROM Bevanda WHERE id = ?";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		ps.setInt(1, ID);
		
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		
		Bevanda temp = new Bevanda();
		temp.setId(ID);
		temp.setNome(risultato.getString(2));
		temp.setCodice(risultato.getString(3));
		temp.setIngredienti(risultato.getString(4));
		temp.setPrezzo(risultato.getFloat(5));
		temp.setVolume(risultato.getFloat(6));
		
		
		return temp;
	}

	@Override
	public ArrayList<Bevanda> getAll() throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		ArrayList<Bevanda> elenco = new ArrayList<Bevanda>();
		
		String query = "SELECT id, nome, ingredienti, codice, prezzo, volume FROM Bevanda";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Bevanda temp = new Bevanda();
			temp.setId(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setCodice(risultato.getString(3));
			temp.setIngredienti(risultato.getString(4));
			temp.setPrezzo(risultato.getFloat(5));
			temp.setVolume(risultato.getFloat(6));
			
			elenco.add(temp);
		}
		return elenco;
	}

	@Override
	public boolean insert(Bevanda t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int ID) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Bevanda update(Bevanda t) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
