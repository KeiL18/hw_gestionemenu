package com.gestioneMenu.HWgestioneMenu.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.gestioneMenu.HWgestioneMenu.connessione.ConnettoreDB;
import com.gestioneMenu.HWgestioneMenu.model.Piatto;
import com.mysql.jdbc.PreparedStatement;

public class PiattoDAO implements Dao<Piatto>{

	@Override
	public Piatto getById(int ID) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT id, nome, ingredienti, codice, prezzo, peso FROM Piatto WHERE id = ?";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		ps.setInt(1, ID);
		
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		
		Piatto temp = new Piatto();
		temp.setId(ID);
		temp.setNome(risultato.getString(2));
		temp.setCodice(risultato.getString(3));
		temp.setIngredienti(risultato.getString(4));
		temp.setPrezzo(risultato.getFloat(5));
		temp.setPeso(risultato.getFloat(6));
		
		
		return temp;
	}

	@Override
	public ArrayList<Piatto> getAll() throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		ArrayList<Piatto> elenco = new ArrayList<Piatto>();
		
		String query = "SELECT id, nome, ingredienti, codice, prezzo, peso FROM Piatto";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Piatto temp = new Piatto();
			temp.setId(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setCodice(risultato.getString(3));
			temp.setIngredienti(risultato.getString(4));
			temp.setPrezzo(risultato.getFloat(5));
			temp.setPeso(risultato.getFloat(6));
			
			elenco.add(temp);
		}

		return elenco;
	}

	@Override
	public boolean insert(Piatto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int ID) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Piatto update(Piatto t) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
