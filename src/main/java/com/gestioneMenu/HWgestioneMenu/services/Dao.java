package com.gestioneMenu.HWgestioneMenu.services;

import java.sql.SQLException;
import java.util.ArrayList;

public interface Dao<T> {
	
	T getById(int ID) throws SQLException;
	
	ArrayList<T> getAll() throws SQLException;
	
	boolean insert(T t) throws SQLException;
	
	boolean delete(int ID)throws SQLException;
	
	T update(T t) throws SQLException;
}
